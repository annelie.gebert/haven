package de.awacademy.haven;

import jakarta.persistence.*;

@Entity
public class CrewMember {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String prename;
    private String secretGift;

    @ManyToOne
    private Ship ship;

    // getter und setter

    public Integer getId() {
        return id;
    }

    // kein setter für id

    public String getPrename() {
        return prename;
    }

    public void setPrename(String prename) {
        this.prename = prename;
    }

    public String getSecretGift() {
        return secretGift;
    }

    public void setSecretGift(String secretGift) {
        this.secretGift = secretGift;
    }

    public Ship getShip() {
        return ship;
    }

    public void setShip(Ship ship) {
        this.ship = ship;
    }
}
