package de.awacademy.haven;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class ShipController {
    private final ServiceRepository serviceRepository;
    public ShipController(ServiceRepository serviceRepository) {

        this.serviceRepository = serviceRepository;

    }

    @GetMapping("/")
    public String showIndex(Model model) {

        List<Ship> shipList = serviceRepository.getShipRepository().findAll();
        List<Ship> shipListsorted = shipList.stream()
                .sorted(Comparator.comparingInt(Ship::getYearBuild))
                .collect(Collectors.toList());

        model.addAttribute("shipList", shipListsorted);

        return "index";
    }
    // ##########################################################################################

    @GetMapping("/createShip")
    public String showCreateShip(Model model) {

        ShipDTO shipDTO = new ShipDTO("", 0);
        model.addAttribute("ship", shipDTO);

        return "createShip";
    }

    @PostMapping("/submitShip")
    public String submitShip(@ModelAttribute ShipDTO shipDTO) {

        Ship ship = new Ship();
        ship.setName(shipDTO.getName());
        ship.setYearBuild(shipDTO.getYearBuild());
        serviceRepository.getShipRepository().save(ship);

        return "redirect:/";
    }

    @GetMapping("shipDetails")
    public String showShipDetails(@RequestParam int shipId, Model model) {

        Ship ship = serviceRepository.getShipRepository().findById(shipId).get();
        model.addAttribute("ship", ship);

        return "shipDetails";
    }
}
