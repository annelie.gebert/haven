package de.awacademy.haven;

public class ShipDTO {

    private String name;
    private Integer yearBuild;

    // #### Konstruktor ####

    public ShipDTO() {
    }

    public ShipDTO(String name, Integer yearBuild) {
        this.name = name;
        this.yearBuild = yearBuild;
    }

    // #### getter und setter ####
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getYearBuild() {
        return yearBuild;
    }

    public void setYearBuild(Integer yearBuild) {
        this.yearBuild = yearBuild;
    }
}
