package de.awacademy.haven;

public class CrewMemberDTO {

    private String prename;
    private String secretGift;

    // Konstruktor

    public CrewMemberDTO() {
    }

    public CrewMemberDTO(String prename, String secretGift) {
        this.prename = prename;
        this.secretGift = secretGift;
    }

    // getter und setter

    public String getPrename() {
        return prename;
    }

    public void setPrename(String prename) {
        this.prename = prename;
    }

    public String getSecretGift() {
        return secretGift;
    }

    public void setSecretGift(String secretGift) {
        this.secretGift = secretGift;
    }
}
