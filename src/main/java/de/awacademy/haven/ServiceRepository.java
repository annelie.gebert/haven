package de.awacademy.haven;

import org.springframework.stereotype.Service;

@Service
public class ServiceRepository {
    private ShipRepository shipRepository;
    private CrewMemberRepository crewMemberRepository;

    public ServiceRepository(ShipRepository shipRepository, CrewMemberRepository crewMemberRepository) {
        this.shipRepository = shipRepository;
        this.crewMemberRepository = crewMemberRepository;
    }

    // getter und setter
    public ShipRepository getShipRepository() {
        return shipRepository;
    }

    public void setShipRepository(ShipRepository shipRepository) {
        this.shipRepository = shipRepository;
    }

    public CrewMemberRepository getCrewMemberRepository() {
        return crewMemberRepository;
    }

    public void setCrewMemberRepository(CrewMemberRepository crewMemberRepository) {
        this.crewMemberRepository = crewMemberRepository;
    }
}
