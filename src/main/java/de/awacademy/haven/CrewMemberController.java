package de.awacademy.haven;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CrewMemberController {

    private ServiceRepository serviceRepository;

    public CrewMemberController(ServiceRepository serviceRepository) {

        this.serviceRepository = serviceRepository;
    }

    @GetMapping("/createCrewMember")
    public String showCreateCrewMember(@RequestParam int shipId, Model model) {

        Ship ship = serviceRepository.getShipRepository().findById(shipId).get();
        CrewMemberDTO crewMemberDTO = new CrewMemberDTO("", "");

        model.addAttribute("crewMember", crewMemberDTO);
        model.addAttribute("ship", ship);
        model.addAttribute("shipId", shipId);

        return "createCrewMember";
    }

    @PostMapping("/submitCrewMember")
    public String submitCrewMember(@ModelAttribute CrewMemberDTO crewMemberDTO, @RequestParam int shipId) {

        Ship ship = serviceRepository.getShipRepository().findById(shipId).get();
        CrewMember crewMember = new CrewMember();
        crewMember.setPrename(crewMemberDTO.getPrename());
        crewMember.setSecretGift(crewMemberDTO.getSecretGift());
        crewMember.setShip(ship);

        serviceRepository.getCrewMemberRepository().save(crewMember);

        return "redirect:/";
    }

    @PostMapping("/deleteCrewMember")
    public String deleteCrewMember(@RequestParam int memberId) {

        serviceRepository.getCrewMemberRepository().deleteById(memberId);

        return "redirect:/";
    }

}
